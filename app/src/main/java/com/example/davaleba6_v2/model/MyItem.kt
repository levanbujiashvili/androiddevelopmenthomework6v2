package com.example.davaleba6_v2.model

class MyItem {
    var title : String? = null
    var description : String? = null

    constructor(){

    }

    constructor(title: String?, description: String?) {
        this.title = title
        this.description = description
    }
}