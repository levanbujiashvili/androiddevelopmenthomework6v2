package com.example.davaleba6_v2.DB

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import com.example.davaleba6_v2.model.MyItem

class DBHelper(context: Context):SQLiteOpenHelper(context, DATABASE_NAME, null, DATABASE_VER) {
    companion object {
        private val DATABASE_VER = 1
        private val DATABASE_NAME = "levan.db"


        private val TABLE_NAME = "Item"
        private val TITLE = "Title"
        private val DESCRIPTION = "Description"
    }

    override fun onCreate(db: SQLiteDatabase?) {
        val CREATE_TABLE_QUERY:String = ("CREATE TABLE"+ TABLE_NAME+"("
                + TITLE +" TEXT," + DESCRIPTION+" TEXT)")
        db!!.execSQL(CREATE_TABLE_QUERY);
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        db!!.execSQL("DROP TABLE IF EXISTS $TABLE_NAME")
        onCreate(db!!)
    }
    val allItems:List<MyItem>
        get() {
            val listItems = ArrayList<MyItem>()
            val selectQuery = "SELECT * FROM $TABLE_NAME"
            val db = this.writableDatabase
            val cursor = db.rawQuery(selectQuery, null)
            if(cursor.moveToFirst())
            {
                do{
                    val myItem = MyItem()
                    myItem.title=cursor.getString(cursor.getColumnIndex(TITLE))
                    myItem.description=cursor.getString(cursor.getColumnIndex(DESCRIPTION))

                    listItems.add(myItem)
                }while (cursor.moveToNext())
            }
            db.close()
            return listItems
        }

    fun addItem(myItem: MyItem)
    {
        val db = this.writableDatabase
        val values = ContentValues()
        values.put(TITLE,myItem.title)
        values.put(DESCRIPTION,myItem.description)

        db.insert(TABLE_NAME, null,values)
        db.close()

    }
    fun updateItem(myItem: MyItem):String
    {
        val db = this.writableDatabase
        val values = ContentValues()
        values.put(TITLE,myItem.title)
        values.put(DESCRIPTION,myItem.description)

//        db.insert(TABLE_NAME, null,values)
//        db.close()
        return db.update(TABLE_NAME,values,"$TITLE=?", arrayOf(myItem.title)).toString()

    }
    fun deleteItem(myItem: MyItem) {
        val db = this.writableDatabase

        db.delete(TABLE_NAME,"$TITLE=?", arrayOf(myItem.title.toString()))
        db.close()

    }
}